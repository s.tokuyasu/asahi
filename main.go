package main

import (
	"asahi/model"
	"strconv"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.LoadHTMLGlob("web/templates/*.html")
	model.DbInit()
	//Index
	router.GET("/", func(ctx *gin.Context) {
		todos := model.DbGetAll()
		ctx.HTML(200, "index.html", gin.H{
			"todos": todos,
		})
	})
	//Create
	router.POST("/new", func(ctx *gin.Context) {
		text := ctx.PostForm("text")
		status := ctx.PostForm("status")
		model.DbInsert(text, status)
		ctx.Redirect(302, "/")
	})
	//Detail
	router.GET("/detail/:id", func(ctx *gin.Context) {
		n := ctx.Param("id")
		id, err := strconv.Atoi(n)
		if err != nil {
			panic(err)
		}
		todo := model.DbGetOne(id)
		ctx.HTML(200, "detail.html", gin.H{"todo": todo})
	})
	//Update
	router.POST("/update/:id", func(ctx *gin.Context) {
		n := ctx.Param("id")
		id, err := strconv.Atoi(n)
		if err != nil {
			panic("ERROR")
		}
		text := ctx.PostForm("text")
		status := ctx.PostForm("status")
		model.DbUpdate(id, text, status)
		ctx.Redirect(302, "/")
	})
	//削除確認
	router.GET("/delete_check/:id", func(ctx *gin.Context) {
		n := ctx.Param("id")
		id, err := strconv.Atoi(n)
		if err != nil {
			panic("ERROR")
		}
		todo := model.DbGetOne(id)
		ctx.HTML(200, "delete.html", gin.H{"todo": todo})
	})
	//Delete
	router.POST("/delete/:id", func(ctx *gin.Context) {
		n := ctx.Param("id")
		id, err := strconv.Atoi(n)
		if err != nil {
			panic("ERROR")
		}
		model.DbDelete(id)
		ctx.Redirect(302, "/")
	})
	router.Run()
}
